# Contributor: Olliver Schinagl <oliver@schinagl.nl>
# Maintainer: Olliver Schinagl <oliver@schinagl.nl>
pkgname=openv2g
pkgver=0.9.5
pkgrel=4
pkgdesc="Implements ISO IEC 15118 and also the DIN 70121 vehicle to grid (V2G) communication interface"
url="https://openv2g.org/"
arch="all"
license="LGPL-3.0-or-later"
options="!check" # No tests available
subpackages="lib$pkgname:libs lib$pkgname-dev"
source="
	0001-Enable-XMLDSIG-codec-support.patch
	0002-Violate-the-spec-less-by-downgrading-to-ASCII-for-UT.patch
	0003-Enable-DIN-codec-including-fragment-support.patch
	makefiles.patch
	$pkgname.pc.in
	$pkgname-$pkgver.zip::https://downloads.sourceforge.net/sourceforge/openv2g/OpenV2G_$pkgver.zip
"

unpack() {
	mkdir -p "$builddir"
	unzip "$srcdir/$pkgname-$pkgver.zip" -d "$builddir"
	# To ensure patches can be applied, convert the \r\n code-base first.
	find "$builddir" -type f -exec dos2unix '{}' \;
}

build() {
	cd Release
	make

	sed \
	    -e "s|@PKGVER@|${pkgver}|g" \
	    "$srcdir/$pkgname.pc.in" > "$builddir/$pkgname.pc"

}

package() {
	depends="lib$pkgname"
	cd Release
	make DESTDIR="$pkgdir/usr" install

	install -D -m 0644 -t "$pkgdir/usr/lib/pkgconfig/" \
	        "$builddir/$pkgname.pc"
}

sha512sums="
0d055188801e0724b78f935bd3de72ad7b673dab894577f255eb895a4871f1e775476adf6c55427c293e85f488140cad51d7ce85a626dbba6bb87c96a3866064  0001-Enable-XMLDSIG-codec-support.patch
a2349e2780ca9053137329434d87b0670cd424f3878c2c738c41021707b7e652c3690ae83bd7c7abf980194a143d1196d75b5a0559783ac6003611df5d14f822  0002-Violate-the-spec-less-by-downgrading-to-ASCII-for-UT.patch
1f17c5b2a50316a3bfe3578119b86fd4612c754cd6eb8d38fc596fe0282bcd68677e379a33bac558cd392f5f8e021a1744bf230aded628bcd1230d1dab5d5348  0003-Enable-DIN-codec-including-fragment-support.patch
41b7b4a5286bc3138040b1fa492dbf49562a148398fba6ddcbd177baa0a767e66adff2076a0f6a96afaa6fe5d3b808ab92f03eab6d3865b7e414870273915d9a  makefiles.patch
94d1f15c33ae7327d76320d5ba35febd6068393e70667729953edfc3a54eb1dc91513bec8abf11b14b53154594746123cca8db00cb31d5a5d139b38054d8a986  openv2g.pc.in
a492012fc1820d97fe679f997404d8ed02cfee85137197dbd8084daf35b078c29b54e87c446dec3a420e6b414a6c40cd8eca0528038ab986c4fc91240d809a02  openv2g-0.9.5.zip
"
